﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AI070.Models.WP03004Master;

namespace AI070.Controllers.WP03013
{
    public class WP03013Controller : Controller
    {
        private WP03004Repository db = new WP03004Repository();

        public ActionResult Index(int id)
        {
            TrainingDTO data = WP03004Repository.GetDetailTraining(id);
            
            return View(data);
        }

        public ActionResult TrainingPortal()
        {
            List<WP03004Master> quest = db.getDataWP03004(1, 1000000, "");

            return View(quest);
        }

    }
}