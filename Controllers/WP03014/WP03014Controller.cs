﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;
using Toyota.Common.Web.Platform;
using AI070.Models;
using AI070.Models.Shared;
using AI070.Models.WP03014Master;
using System.Security.Cryptography;
using System.Text;

namespace AI070.Controllers
{
    public class WP03014Controller : PageController
    {
        Message M = new Message();
        WP03014Repository R = new WP03014Repository();
        User U = new User();
        string username;
        string MESSAGE_TXT;
        string MESSAGE_TYPE;

        #region Startup
        protected override void Startup()
        {
            try
            {
                Settings.Title = "Worker In Project";
                ViewData["Title"] = Settings.Title;
                GetDataHeader();
            }
            catch (Exception e)
            {
                Response.Redirect("authorized");
            }
        }
        #endregion

        #region Generate Message
        public ActionResult GenerateMessage(string MSG_ID, string p_PARAM1, string p_PARAM2, string p_PARAM3, string p_PARAM4)
        {
            try
            {
                M.MSG_ID = MSG_ID;
                M.p_PARAM1 = p_PARAM1;
                M.p_PARAM2 = p_PARAM2;
                M.p_PARAM3 = p_PARAM3;
                M.p_PARAM4 = p_PARAM4;
                var res = M.getMessageTextWithFunctionSQL(M);
                MESSAGE_TXT = res[0].MSG_TEXT;
                MESSAGE_TYPE = res[0].MSG_TYPE;
            }
            catch (Exception M)
            {
                MESSAGE_TXT = M.Message.ToString();
                MESSAGE_TYPE = "Err";
            }
            return Json(new { MESSAGE_TXT, MESSAGE_TYPE }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Data Header
        public void GetDataHeader()
        {
            try
            {

            }
            catch (Exception M)
            {
                M.Message.ToString();
            }
        }
        #endregion

        #region Search Data
        public ActionResult Search_Data(
                                        int start,
                                        int display,
                                        string employeeName,
                                        string projectName,
                                        string companyName
                                        )
        {
            //Buat Paging//
            PagingModel_WP03014 pg = new PagingModel_WP03014(
                                                              R.GetCountDataProject(employeeName, projectName, companyName),
                                                              start,
                                                              display
                                                            );

            //Munculin Data ke Grid//
            List<WP03014Master> List = R.GetDataProjectByFilter(pg.StartData, pg.EndData, employeeName, projectName, companyName);
            ViewData["DataWP03014"] = List;
            ViewData["PagingWP03014"] = pg;

            return PartialView("Datagrid_Data", pg.CountData);
        }
        #endregion

        #region Add Or Update
        [HttpGet]
        public virtual ActionResult AddOrUpdate(int id)
        {
            WP03014Master data = R.GetDataProjectById(id);
            List<SelectListItem> masterCompany = R.GetCompanyList().Select(x => new SelectListItem()
                {
                    Value = x.Id.ToString(),
                    Text = x.CompanyName
                }).ToList();

            List<SelectListItem> masterProjects = R.GetProjectList().Select(x => new SelectListItem()
                {
                    Value = x.Id.ToString(),
                    Text = x.ProjectName
                }).ToList();


            masterCompany.Insert(0, new SelectListItem { Text = "--Select Company--", Value = "" });
            masterProjects.Insert(0, new SelectListItem { Text = "--Select Project--", Value = "" });

            if (id == 0)
            {
                Settings.Title = "Add Project Worker";
            } else
            {
                Settings.Title = "Edit Project Worker";
            }
            
            ViewData["Title"] = Settings.Title;
            ViewData["CompanyList"] = masterCompany;
            ViewData["ProjectList"] = masterProjects;

            return PartialView("ADD_EDIT",data);
        }
        #endregion

        [HttpPost]
        public virtual ActionResult AddOrUpdate(WP03014Master DataProject)
        {
            string sts = null;
            string message = null;
            DataProject.EmployeeId = int.Parse(Session["ID_TB_M_EMPLOYEE"].ToString());
            DataProject.EmployeeName = R.GetEmployeeById(DataProject.EmployeeId).EmployeeName;

            try
            {

                if (DataProject.Id == 0)
                {
                    DataProject.CreatedBy = DataProject.EmployeeName;
                    DataProject.IsDeleted = 0;
                    WP03014Repository.Insert(DataProject);
                } else
                {
                    DataProject.ModifiedBy = DataProject.EmployeeName;
                    WP03014Repository.Update(DataProject);
                }
            }
            catch (Exception M)
            {
                sts = "false";
                message = M.Message.ToString();
            }

            return Redirect(Request.UrlReferrer.ToString());

        }

        #region Delete Data
        public ActionResult Delete(string inlineId)
        {
            string stsRespon;
            var sts = new object();
            string message = null;
            username = Lookup.Get<Toyota.Common.Credential.User>().Username.ToString();

            try
            {
                var Datas = inlineId.Split(',');
                for (int i = 0; i < Datas.Count(); i++)
                {
                    if (Datas[i] != "")
                    {
                        R.Delete_Data(Datas[i]);
                    }
                }

                sts = "TRUE";
                message = "Data has been successfully Deleted";
            }
            catch (Exception M)
            {
                sts = "false";
                message = M.Message.ToString();
            }
            return Json(new { sts, message }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
