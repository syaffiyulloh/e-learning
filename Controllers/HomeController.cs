﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using AI070.Models;

namespace AI070.Controllers
{
    public class HomeController : BaseController
    {
        protected override void Startup()
        {
            Settings.Title = "Dashboard";
  
            if (!ApplicationSettings.Instance.Security.SimulateAuthenticatedSession)
            {
                ViewData["ListFunction"] = AppRepository.Instance.getApps(AppRepository.Instance.countApps());
            }
            else
            {
                ViewData["ListFunction"] = null;
            }
            ViewData["UserInfo"] = UserInfo;

        }

        public ActionResult WidgetSettings()
        {
            return PartialView("_WidgetSettings");
        }

    }
}
