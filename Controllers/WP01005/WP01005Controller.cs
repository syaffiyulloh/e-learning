﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;
using Toyota.Common.Web.Platform;
using AI070.Models;
using AI070.Models.Shared;
using AI070.Models.WP01005Master;
using System.Security.Cryptography;
using System.Text;

namespace AI070.Controllers
{
    public class WP01005Controller : PageController
    {

        ResultMessage rm = new ResultMessage();
        Message M = new Message();
        WP01005Repository R = new WP01005Repository();
        User U = new User();
        string username;
        string MESSAGE_TXT;
        string MESSAGE_TYPE;

        protected override void Startup()
        {
            try
            {
                Settings.Title = "User / Worker Master";
                ViewData["Title"] = Settings.Title;
                GetDataHeader();
            }
            catch (Exception e)
            {
                Response.Redirect("authorized");
            }
        }

        public ActionResult GenerateMessage(string MSG_ID, string p_PARAM1, string p_PARAM2, string p_PARAM3, string p_PARAM4)
        {
            try
            {
                M.MSG_ID = MSG_ID;
                M.p_PARAM1 = p_PARAM1;
                M.p_PARAM2 = p_PARAM2;
                M.p_PARAM3 = p_PARAM3;
                M.p_PARAM4 = p_PARAM4;
                var res = M.getMessageTextWithFunctionSQL(M);
                MESSAGE_TXT = res[0].MSG_TEXT;
                MESSAGE_TYPE = res[0].MSG_TYPE;
            }
            catch (Exception M)
            {
                MESSAGE_TXT = M.Message.ToString();
                MESSAGE_TYPE = "Err";
            }
            return Json(new { MESSAGE_TXT, MESSAGE_TYPE }, JsonRequestBehavior.AllowGet);
        }

        #region Data Header
        public void GetDataHeader()
        {
            try
            {
                ViewData["COMPANY"] = R.getCompany();
                ViewData["EXECUTOR"] = R.getExecutor();
                ViewData["IDENTITY"] = R.getIdentity();
                ViewData["SECTION"] = R.getSection("TMMIN");
                ViewData["PIC"] = R.getPIC();
            }
            catch (Exception M)
            {
                M.Message.ToString();
            }
        }
        #endregion

        #region Section
        public ActionResult Test(string SYSTEM_CD)
        {
            var sts = new object();
            var message = new object();
            try
            {
                var Exec = R.getExecutor();
                sts = "TRUE";
                message = Exec;
            }
            catch (Exception M)
            {
                sts = "Err";
                message = M.Message.ToString();
            }
            return Json(new { sts, message }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Section
        public ActionResult GetDataSection(string SYSTEM_CD)
        {
            var sts = new object();
            var message = new object();
            try
            {
                var Exec = R.getSection(SYSTEM_CD);
                sts = "TRUE";
                message = Exec;
            }
            catch (Exception M)
            {
                sts = "Err";
                message = M.Message.ToString();
            }
            return Json(new { sts, message }, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region PIC
        public ActionResult GetDataPIC(string SYSTEM_CD)
        {
            var sts = new object();
            var message = new object();
            try
            {
                var Exec = R.getPIC();
                sts = "TRUE";
                message = Exec;
            }
            catch (Exception M)
            {
                sts = "Err";
                message = M.Message.ToString();
            }
            return Json(new { sts, message }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Search Data
        public ActionResult Search_Data(int start, int display, string DATA_ID, string EXECUTION_TIME, string TIME_UNIT_CRITERIA, string STATUS_ACTIVE, string EMPLOYEE_NAME)
        {
            //Buat Paging//
            PagingModel_WP01005 pg = new PagingModel_WP01005(R.getCountWP01005(DATA_ID, EXECUTION_TIME, TIME_UNIT_CRITERIA, STATUS_ACTIVE, EMPLOYEE_NAME), start, display);

            //Munculin Data ke Grid//
            List<WP01005Master> List = R.getDataWP01005(pg.StartData, pg.EndData, EMPLOYEE_NAME).ToList();
            ViewData["DataWP01005"] = List;
            ViewData["PagingWP01005"] = pg;
            return PartialView("Datagrid_Data", pg.CountData);
        }
        #endregion

        #region Add New

        string HashMd5(HashAlgorithm md5Hash, string input)
        {
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
        string EncryptPassword(string value)
        {
            using (var md5 = MD5.Create())
            {
                return HashMd5(md5, value);
            }
        }
        public ActionResult ADD_NEW(string COMPANY, string REGNUMBER, string USERNAME, string PASSWORD, string EMAIL, string SECTION, string FIRSTNAME, string LASTNAME,
            string ADDRESS, string PHONE, string IDENTITY_TYPE, string IDENTITY_NO, string PIC_STATUS, string ANZEN_NO, string ANZEN_DT_FROM, string ANZEN_DT_TO, string SAFETY_INDUCTION_NO, string SAFETY_INDUCTION_FROM, string SAFETY_INDUCTION_TO, string SEQ_NO, string STATUS)
        {
            string sts = null;
            string message = null;
            username = Lookup.Get<Toyota.Common.Credential.User>().Username.ToString();
            string pass = EncryptPassword(PASSWORD);
            try
            {
                string username = Lookup.Get<Toyota.Common.Credential.User>().Username;
                var Exec = WP01005Repository.Create(COMPANY, REGNUMBER,USERNAME, pass, EMAIL, SECTION, FIRSTNAME, LASTNAME, ADDRESS, PHONE, IDENTITY_TYPE, IDENTITY_NO, PIC_STATUS, ANZEN_NO, ANZEN_DT_FROM, ANZEN_DT_TO, SAFETY_INDUCTION_NO, SAFETY_INDUCTION_FROM, SAFETY_INDUCTION_TO, SEQ_NO, STATUS, username);
                sts = Exec[0].STACK;

                if (Exec[0].LINE_STS == "DUPLICATE")
                {
                    var res = M.get_default_message("MWP004", "User / Worker Master", "", "");
                    message = res[0].MSG_TEXT;
                }
                else if (Exec[0].STACK == "TRUE")
                {
                    var res = M.get_default_message("MWP001", "User / Worker Master", "", "");
                    message = res[0].MSG_TEXT;
                }
                else
                {
                    message = Exec[0].LINE_STS;
                }
            }
            catch (Exception M)
            {
                sts = "false";
                message = M.Message.ToString();
            }
            return Json(new { sts, message }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update Data
        public ActionResult Update_Data(string DATA)
        {
            string stsRespon;
            var sts = new object();
            var message = new object();
            username = Lookup.Get<Toyota.Common.Credential.User>().Username.ToString();

            try
            {
                string username = Lookup.Get<Toyota.Common.Credential.User>().Username;
                var Datas = DATA.Split(',');
                string ID = Datas[0];
                string NOREG = Datas[1];
                string COMPANY = Datas[2];
                string FIRSTNAME = Datas[3];
                string LASTNAME = Datas[4];
                string USERNAME = Datas[5];
                string PASSWORD = Datas[6];
                string EMAIL = Datas[7];
                string ADDRESS = Datas[8];
                string PHONE = Datas[9];
                string IDENTITY_TYPE = Datas[10];
                string IDENTITY_NO = Datas[11];
                string PIC_STATUS = Datas[12];
                string ANZEN_NO = Datas[13];
                string ANZEN_DT_FROM = Datas[14];
                string ANZEN_DT_TO = Datas[15];
                string SAFETY_INDUCTION_NO = Datas[16];
                string SAFETY_INDUCTION_FROM = Datas[17];
                string SAFETY_INDUCTION_TO = Datas[18];
                string SECTION = Datas[19];
                string STATUS = "1";
                var EXEC = R.Update_Data(ID, NOREG, COMPANY, FIRSTNAME, LASTNAME, USERNAME, PASSWORD, EMAIL, ADDRESS, PHONE, IDENTITY_TYPE, IDENTITY_NO, PIC_STATUS, ANZEN_NO, ANZEN_DT_FROM, ANZEN_DT_TO, SAFETY_INDUCTION_NO, SAFETY_INDUCTION_FROM, SAFETY_INDUCTION_TO, SECTION, STATUS, username);
                sts = EXEC[0].STACK;
                if (EXEC[0].STACK == "TRUE")
                {
                    var res = M.get_default_message("MWP003", "User / Worker Master", "", "");
                    message = res[0].MSG_TEXT;
                }
                else
                {
                    message = EXEC[0].LINE_STS;
                }
                

            }
            catch (Exception M)
            {
                sts = "false";
                message = M.Message.ToString();
            }
            return Json(new { sts, message }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Delete Data
        public ActionResult Delete_Data(string DATA)
        {
            string stsRespon;
            var sts = new object();
            string message = null;
            username = Lookup.Get<Toyota.Common.Credential.User>().Username.ToString();

            try
            {
                var Datas = DATA.Split(',');
                for (int i = 0; i < Datas.Count(); i++)
                {
                    if (Datas[i] != "")
                    {
                        R.Delete_Data(Datas[i]);
                    }
                }

                sts = "TRUE";
                var res = M.get_default_message("MWP002", "User / Worker Master", "", "");
                message = res[0].MSG_TEXT;
            }
            catch (Exception M)
            {
                sts = "false";
                message = M.Message.ToString();
            }
            return Json(new { sts, message }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
