﻿using AI070.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;

namespace AI070.Controllers
{
    public class LoginController : LoginPageController
    {
        public ActionResult CheckUserPlantMapping(string username)
        {

            int isError = SystemRepository.Instance.CheckUserPlantMapping(username);

            return Json(new
            {
                isError = isError
            });
        }
    }
}
