﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;

namespace AI070.Controllers
{
    public class BlankController : BaseController
    {
        protected override void Startup()
        {
            Settings.Title = "Blank";
        }

    }
}
