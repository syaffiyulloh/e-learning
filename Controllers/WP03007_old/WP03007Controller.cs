﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AI070.Models.WP03007;

namespace AI070.Controllers.WP03007
{
    public class WP03007Controller : Controller
    {
        private WP03007Repository db = new WP03007Repository();

        public ActionResult Index(int UserId, int SubjectId)
        {
            MasterEmployee userData = db.GetMasterEmployee(UserId);
            MasterExamSubject quest = db.GetExamSubjectActive(SubjectId);
            List<MasterExamQuestion> data = db.GetExamQuestionActive(SubjectId).OrderBy(x => x.Id).ToList();

            if (quest.ExamType == "Random")
            {
                Random rng = new Random();
                int n = data.Count;
                while (n > 1)
                {
                    n--;
                    int k = rng.Next(n + 1);
                    MasterExamQuestion value = data[k];
                    data[k] = data[n];
                    data[n] = value;
                }
            }

            ViewBag.Breadcum = quest.Title;
            ViewBag.User = (userData.Name);
            ViewData["SubjectId"] = quest.Id;
            ViewData["UserId"] = userData.Id;
            ViewData["examDuration"] = (int)quest.ExamDuration;
            return View(data);
        }

        public ActionResult Finish(int UserId)
        {
            MasterEmployee userData = db.GetMasterEmployee(UserId);

            ViewBag.User = (userData.Name);
            ViewData["UserId"] = userData.Id;
            return View();
        }
        public ActionResult ExamPortal()
        {
            int UserId = int.Parse(Session["ID_TB_M_EMPLOYEE"].ToString());

            MasterEmployee userData = db.GetMasterEmployee( UserId);
            List<MasterExamSubject> quest = db.GetListExamSubjectActive(UserId);

            ViewBag.User = (userData.Name);
            ViewData["UserId"] = userData.Id;
            return View(quest);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Step(List<ExamAnswerDTO> answerList, int subjectId, int userId, int timeElapased)
        {

            MasterEmployee employeData = db.GetMasterEmployee(userId);
            MasterExamSubject subjectData = db.GetExamSubjectActive(subjectId);
            List<MasterExamQuestion> data = db.GetExamQuestionActive(subjectId);

            var compar = data
                    .Join(
                        answerList,
                        m => m.Id,
                        r => r.Id,
                        (m, r) => new
                        {
                            r.Id,
                            r.No,
                            realKey = m.AnswerUserChoose,
                            answer = r.AnswerUserChoose,
                            choose = (r.AnswerUserChoose == m.AnswerChoice_A ? "A" 
                                    : (r.AnswerUserChoose == m.AnswerChoice_B ? "B" 
                                    : (r.AnswerUserChoose == m.AnswerChoice_C ? "C"
                                    : (r.AnswerUserChoose == m.AnswerChoice_D ? "D"
                                    : (r.AnswerUserChoose == m.AnswerChoice_E ? "E"
                                    : "NOT_ANSWER")))))
                        }
                    ).ToList();

            int varTrue = 0;
            int varFalse = 0;
            int varNull = 0;

            string keterangan = String.Empty;


            foreach (var i in compar)
            {
                keterangan = keterangan + ", " + i.Id + "_" + i.No + "_" + i.choose;

                if (String.IsNullOrWhiteSpace(i.answer))
                {
                    varNull++;
                }
                else if (i.answer.Equals(i.realKey))
                {
                    varTrue++;
                }
                else
                {
                    varFalse++;
                }
            }

            keterangan = keterangan.Remove(0, 2);

            int varTot = varTrue + varFalse + varNull; //total yg dijawab
            string status = ((varTrue * 100) / varTot) > subjectData.PassingScore ? "1" : "0";

            db.CreateExamScore(new MasterExamScore
            {
                EmployeeId = employeData.Id,
                SubjectId = subjectData.Id,
                CompanyId = employeData.CompanyId,
                Answer = keterangan,
                CorrectAmount = varTrue,
                WrongAmount = varFalse,
                NotAnswerAmount = varNull,
                PassGraduate = subjectData.PassingScore.ToString(),
                Score = ((varTrue * 100) / varTot),
                Timer = (int)(timeElapased / 60),
                SubmitDate = DateTime.Now,
                Status = status
            });

            return Json("Sukses");
        }

    }
}