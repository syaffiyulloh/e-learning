﻿using System;
using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models
{
    public class UserInfo
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Company { get; set; }
        public string PlantName { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string LocationDisplay { get; set; }
    }
}