﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP0102Master
{
    public class WP0102
    {
        public string PROJECT_CD { get; set; }
        public string JOBS { get; set; }
        public string DANGERLEVEL { get; set; }
        public string CATEGORY { get; set; }

        // untuk combobox Time 
        public string TIME_CD { get; set; }
        public string TIME_VAL { get; set; }

        // untuk combobox Status
        public string STS_CD { get; set; }
        public string STS_VAL { get; set; }
        public string STACK { get; set; }
        public string LINE_STS { get; set; }

    }

}