﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP03009
{
    public partial class MasterExamScore
    {
        public int Id { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public string Answer { get; set; }
        public int CorrectAmount { get; set; }
        public int WrongAmount { get; set; }
        public int NotAnswerAmount { get; set; }
        public Nullable<short> Remedial { get; set; }
        public int Score { get; set; }
        public string PassGraduate { get; set; }
        public Nullable<int> Timer { get; set; }
        public Nullable<System.DateTime> SubmitDate { get; set; }
        public string Status { get; set; }
    }

}