﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using Toyota.Common.Credential;
using AI070.Models;

namespace AI070.Models.WP03014Master
{
    public class WP03014Repository
    {
        public WP03014Master GetDataProjectById(int id)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP03014Master>("WP03014/WP03014_SearchDataById", new
            {
                id
            });
            db.Close();
            return d.FirstOrDefault();
        }

        #region Get_Data_Grid_WP03014
        public List<WP03014Master> GetDataProjectByFilter(
                                                    int PageNumber,
                                                    int Display,
                                                    string EmployeeName,
                                                    string ProjectName,
                                                    string CompanyName
                                                  )
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP03014Master>("WP03014/WP03014_SearchData", new
            {
                PageNumber,
                Display,
                EmployeeName,
                ProjectName,
                CompanyName
            });
            db.Close();
            return d.ToList();
        }
        #endregion

        #region Count_Get_Data_Grid_WP03014
        public int GetCountDataProject(string EmployeeName, string ProjectName, string CompanyName)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();
            int result = db.SingleOrDefault<int>("WP03014/WP03014_SearchCount", new {
                EmployeeName,
                ProjectName,
                CompanyName
            });
            db.Close();
            return result;
        }
        #endregion

        #region Delete Data
        public void Delete_Data(string id)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Execute("WP03014/WP03014_Delete", new
            {
                id
            });
            db.Close();
        }
        #endregion

        #region Update Data
        public static void Update(WP03014Master dataProject)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<QueryResult>("WP03014/WP03014_Update", new
            {
                dataProject.Id,
                dataProject.EmployeeId,
                dataProject.CompanyId,
                dataProject.ProjectId,
                dataProject.CompanyFromId,
                dataProject.CompanyToId,
                dataProject.IsProgress,
                dataProject.ModifiedBy
            });
            db.Close();
        }
        #endregion

        #region Insert Data
        public static void Insert(WP03014Master dataProject)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<QueryResult>("WP03014/WP03014_Insert", new
            {
                dataProject.EmployeeId,
                dataProject.CompanyId,
                dataProject.ProjectId,
                dataProject.CompanyFromId,
                dataProject.CompanyToId,
                dataProject.IsProgress,
                dataProject.IsDeleted,
                dataProject.CreatedBy,
            });
            db.Close();
        }
        #endregion

        public List<MasterCompany> GetCompanyList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<MasterCompany>("WP03014/WP03014_GetCompanyList", new{});
            db.Close();
            return d.ToList();
        }

        public List<MasterProject> GetProjectList()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<MasterProject>("WP03014/WP03014_GetProjectList", new { });
            db.Close();
            return d.ToList();
        }

        public MasterEmployee GetEmployeeById(int Id)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<MasterEmployee>("WP03014/WP03014_GetEmployeeById", new { Id });
            db.Close();
            return d.FirstOrDefault();
        }

    }

    #region Paging Model
    public class PagingModel_WP03014
    {
        public int CountData { get; set; }
        public int StartData { get; set; }
        public int EndData { get; set; }
        public int PositionPage { get; set; }
        public List<int> ListIndex { get; set; }
        public PagingModel_WP03014(int countdata, int positionpage, int dataperpage)
        {
            List<int> list = new List<int>();
            EndData = positionpage * dataperpage;
            CountData = countdata;
            PositionPage = positionpage;
            StartData = (positionpage - 1) * dataperpage + 1;
            Double jml = countdata / dataperpage;
            if (countdata % dataperpage > 0)
            {
                jml = jml + 1;
            }

            for (int i = 0; i < jml; i++)
            {
                list.Add(i);
            }
            ListIndex = list;
        }
    }
    #endregion

    #region Query Result
    public class QueryResult
    {
        public string STACK { get; set; }
        public string LINE_STS { get; set; }
    }
    #endregion

}
