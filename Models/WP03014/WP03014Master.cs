﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AI070.Models.WP03014Master
{
    public class WP03014Master
    {
        public int Id { get; set; }
        public int No { get; set; }
        public int EmployeeId { get; set; }

        [Display(Name = "Employee Name")]
        public string EmployeeName { get; set; }
        public int ProjectId { get; set; }

        [Display(Name = "Project")]
        public string ProjectName { get; set; }
        public int CompanyId { get; set; }

        [Display(Name = "Company")]
        public string CompanyName { get; set; }

        [Display(Name = "Date Join Project")]
        public DateTime DateJoinProject { get; set; }
        public int CompanyFromId { get; set; }

        [Display(Name = "Company From")]
        public string CompanyFromName { get; set; }
        public int CompanyToId { get; set; }

        [Display(Name = "Company To")]
        public string CompanyToName { get; set; }

        [Display(Name = "Is Progress")]
        public string IsProgress { get; set; }
        public int IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        
    }

    public class MasterCompany
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
    }

    public class MasterProject
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
    }

    public class MasterEmployee
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
    }
}