﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP03008Master
{
    public class WP03008Master
    {
        public int ROW_NUM { get; set; }
        public string ID { get; set; }
        public string USERNAME { get; set; }
        public string REG_NO { get; set; }
        public string EMAIL { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string IDENTITY_TYPE { get; set; }
        public string IDENTITY_NO { get; set; }
        public string SAFETY_INDUCTION_NO { get; set; }
        public string SI_FROM { get; set; }
        public string SI_TO { get; set; }
        
    }

}