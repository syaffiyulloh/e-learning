﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP0101Master
{
    public class WP0101Master
    {
        // untuk combobox Time 
        public Int64 ID { get; set; }
        public string PROJECT_CD { get; set; }
        public string PROJECT_NAME { get; set; }
        public string DATE { get; set; }
        public string LOCATION { get; set; }
        public string TANGGAL_PELAKSANAAN { get; set; }
        public string JAM_PELAKSANAAN { get; set; }
        public string DIVISION { get; set; }
        public string PELAKSANA { get; set; }
        public string NAMA_KONTRAKTOR { get; set; }
        public string NAMA_PIMPINAN { get; set; }
        public string NAMA_PENGAWAS { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DT { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DT { get; set; }
        public string STATUS { get; set; }
        public string STATUS_ID { get; set; }
        public Int32 ROW_NUM { get; set; }
        public Int32 Number { get; set; }
        public string DELETE_DATA { get; set; }
        public string UPLOADED_BY { get; set; }
        public string UPLOADED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
        public string STACK { get; set; }
        public string LINE_STS { get; set; }

    }

}