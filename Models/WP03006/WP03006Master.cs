﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP03006
{
	public class WP03006Master
	{
        public int Id { get; set; }
        public int RowNum { get; set; }
        public string Subject { get; set; }
        public string EmployeeName { get; set; }
        public string CompanyName { get; set; }
        public int TotalQuestion { get; set; }
        public int CorrectAnswere { get; set; }
        public int InCorrectAnswere { get; set; }
        public int UnassignQuestion { get; set; }
        public int Remedial { get; set; }
        public int Score { get; set; }
        public int PassGraduateScore { get; set; }
        public int TimeSubmit { get; set; }
        public DateTime SubmitDate { get; set; }
        public string ExamStatus { get; set; }
    }

    public class WP03006Relation
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public int EmployeeId { get; set; }
        public int CompanyId { get; set; }
    }

    public class KeyList
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Company { get; set; }

        public string IdentityNo { get; set; }
    }
}