﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP01007Master
{
    public class WP01007Master
    {
        public string ID { get; set; }
        public string ITEM_NAME { get; set; }
        public string ITEM_TYPE { get; set; }
        public string ITEM_TYPE_VAL { get; set; }
        public string ITEM_ST { get; set; }
        public string ITEM_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string UPDATED_BY { get; set; }
        public string UPDATED_DT { get; set; }
        public string STATUS { get; set; }
        public string STATUS_ID { get; set; }
        public Int32 ROW_NUM { get; set; }
        public Int32 Number { get; set; }
        public string DELETE_DATA { get; set; }
        public string STACK { get; set; }
        public string LINE_STS { get; set; }
    }

}