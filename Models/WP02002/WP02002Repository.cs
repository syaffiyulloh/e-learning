﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using Toyota.Common.Credential;
using AI070.Models;

namespace AI070.Models.WP02002Master
{
    public class WP02002Repository
    {
        #region Get_Data_Grid_WP02002
        public List<WP02002Master> getDataWP02002(int Start, int Display, string PROJECT_CODE)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP02002Master>("WP02002/WP02002_SearchData", new
            {
                START = Start,
                DISPLAY = Display,
                PROJECT_CODE
            });
            db.Close();
            return d.ToList();
        }
        #endregion

        #region Count_Get_Data_Grid_WP02002
        public int getCountWP02002(string DATA_ID, string TIME_UNIT_CRITERIA, string EXECUTION_TIME, string STATUS_ACTIVE, string PROJECT_CODE)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();
            int result = db.SingleOrDefault<int>("WP02002/WP02002_SearchCount", new
            {
                DATA_ID = DATA_ID,
                TIME_UNIT_CRITERIA = TIME_UNIT_CRITERIA,
                EXECUTION_TIME = EXECUTION_TIME,
                STATUS_ACTIVE = STATUS_ACTIVE,
                PROJECT_CODE
            });
            db.Close();
            return result;
        }
        #endregion

        #region Delete Data
        public void Delete_Data(string ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Execute("WP02002/WP02002_Delete", new
            {
                ID
            });
            db.Close();
        }
        #endregion

        #region Update Data
        public List<WP02002> Update_Data(string ID, string PROJECT_CODE, string JOBS, string LOWLEVEL, string MEDIUMLEVEL, string HIGHLEVEL, string DATE, string CAT_A, string CAT_B, string CAT_C, string CAT_D, string CAT_E, string CAT_F, string REMARKS, string USERNAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP02002>("WP02002/WP02002_Update", new
            {
                ID,
                PROJECT_CODE,
                JOBS,
                LOWLEVEL,
                MEDIUMLEVEL,
                HIGHLEVEL,
                DATE,
                CAT_A,
                CAT_B,
                CAT_C,
                CAT_D,
                CAT_E,
                CAT_F,
                REMARKS,
                USERNAME
            });
            db.Close();
            return d.ToList();
        }
        #endregion

        public static List<WP02002> Create(string PROJECT_CODE, string LOCATION, string JOBS, string DANGERLEVEL, string DATE, string CATA, string CATB, string CATC, string CATD, string CATE, string CATF, string REMARKS, string USERNAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP02002>("WP02002/WP02002_Create", new
            {
               PROJECT_CODE,
               LOCATION,
               JOBS,
               DANGERLEVEL,
               DATE,
               CATA,
               CATB,
               CATC,
               CATD,
               CATE,
               CATF,
               REMARKS,
               USERNAME
            });
            db.Close();
            return d.ToList();
        }

        #region Get Area
        public List<AreaModel> getArea()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<AreaModel>("WP02002/WP02002_getArea");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Company
        public List<CompanyModel> getCompany()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<CompanyModel>("WP02002/WP02002_getCompany");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Executor
        public List<ExecutorModel> getExecutor()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<ExecutorModel>("WP02002/WP02002_getExecutor");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Division
        public List<DivisionModel> getDivision()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<DivisionModel>("WP02002/WP02002_getDivision");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Project Location
        public static List<LocationModel> getLocation(string PROJECT_CODE)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<LocationModel>("WP02002/WP02002_getLocation", new { 
                PROJECT_CODE
            });

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Project Code
        public List<ProjectCodeModel> getProjectCode()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<ProjectCodeModel>("WP02002/WP02002_getProjectCode");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Jobs
        public List<JobsModel> getJobs()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<JobsModel>("WP02002/WP02002_getJobs");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Data Location
        public List<LocationModel> getDataLocation()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<LocationModel>("WP02002/WP02002_getDataLocation");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Category
        public List<CategoryModel> getCategory()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<CategoryModel>("WP02002/WP02002_getCategory");

            db.Close();
            return d.ToList();
        }
        #endregion


        #region Get Status
        public List<StatusModel> getStatus()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<StatusModel>("WP02002/WP02002_getStatus");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Project Location
        public List<LocationModel> getLocation()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<LocationModel>("WP02002/WP02002_getLocation");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Employee
        public List<EmployeeModel> getEmployee()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<EmployeeModel>("WP02002/WP02002_getEmployee");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Pengawas
        public List<PengawasModel> getPengawas()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<PengawasModel>("WP02002/WP02002_getPengawas");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Pengawas
        public List<PicModel> getPic()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<PicModel>("WP02002/WP02002_getPic");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get WorkingType
        public List<WorkingTypeModel> getWorkingType()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WorkingTypeModel>("WP02002/WP02002_getWorkingType");

            db.Close();
            return d.ToList();
        }
        #endregion
    }

    public class StatusModel
    {
        public string ID { get; set; }
        public string STATUS { get; set; }
    }

    public class ProjectCodeModel
    {
        public string PROJECT_CODE { get; set; }
    }

    public class JobsModel
    {
        public string ID_TB_M_WORKING_TYPE { get; set; }
        public string WORKING_NAME { get; set; }

    }

    public class CategoryModel
    {
        public string CATEGORY { get; set; }
    }

    public class WorkingTypeModel
    {
        public int ID_TB_M_WORKING_TYPE { get; set; }
        public string WORKING_NAME { get; set; }
    }

    public class DivisionModel
    {
        public string Division { get; set; }
    }

    public class WorkingHoursModel
    {
        public string WorkingHours { get; set; }
    }

    public class LocationModel
    {
        public int ID_TB_M_LOCATION { get; set; }
        public int ID_TB_M_AREA { get; set; }
        public string LOC_CD { get; set; }
        public string LOC_NAME { get; set; }
    }

    public class ExecutorModel
    {
        public string Executor_ID { get; set; }
        public string Executor { get; set; }
    }

    public class AreaModel
    {
        public int ID_TB_M_AREA { get; set; }
        public string AREA_NAME { get; set; }
    }

    public class PicModel
    {
        public int ID_TB_M_EMPLOYEE { get; set; }
        public int ID_TB_M_COMPANY { get; set; }
        public string NAME { get; set; }
        public string PHONE { get; set; }
        public string PIC_STATUS { get; set; }
        public string ANZEN_SERTIFICATE_NO { get; set; }
        public string REG_NO { get; set; }
        public string SECTION { get; set; }
    }

    public class CompanyModel
    {
        public int ID_TB_M_COMPANY { get; set; }
        public string COMPANY_CODE { get; set; }
        public string COMPANY_NAME { get; set; }
    }

    public class EmployeeModel
    {
        public int ID_TB_M_EMPLOYEE { get; set; }
        public int ID_TB_M_COMPANY { get; set; }
        public string NAME { get; set; }
        public string PHONE { get; set; }
        public string PIC_STATUS { get; set; }
        public string ANZEN_SERTIFICATE_NO { get; set; }


    }

    public class PengawasModel
    {
        public int ID_TB_M_EMPLOYEE { get; set; }
        public int ID_TB_M_COMPANY { get; set; }
        public string NAME { get; set; }
        public string PHONE { get; set; }
        public string PIC_STATUS { get; set; }
        public string ANZEN_SERTIFICATE_NO { get; set; }
        public string REG_NO { get; set; }
        public string SECTION { get; set; }


    }

    public class PagingModel_WP02002
    {
        public int CountData { get; set; }
        public int StartData { get; set; }
        public int EndData { get; set; }
        public int PositionPage { get; set; }
        public List<int> ListIndex { get; set; }
        public PagingModel_WP02002(int countdata, int positionpage, int dataperpage)
        {
            List<int> list = new List<int>();
            EndData = positionpage * dataperpage;
            CountData = countdata;
            PositionPage = positionpage;
            StartData = (positionpage - 1) * dataperpage + 1;
            Double jml = countdata / dataperpage;
            if (countdata % dataperpage > 0)
            {
                jml = jml + 1;
            }

            for (int i = 0; i < jml; i++)
            {
                list.Add(i);
            }
            ListIndex = list;
        }
    }
}