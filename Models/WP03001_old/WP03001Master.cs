﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP03001Master
{
    public class WP03001Master
    {
        public Int32 ROW_NUM { get; set; }
        public Int32 ID { get; set; }
        public string QUESTION { get; set; }
        public string ANSWER_CHOICE_A { get; set; }
        public string ANSWER_CHOICE_B { get; set; }
        public string ANSWER_CHOICE_C { get; set; }
        public string ANSWER_CHOICE_D { get; set; }
        public string ANSWER_CHOICE_E { get; set; }
        public string ANSWER_KEY { get; set; }
        public string IMAGE { get; set; }
        public string IS_DELETED { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string UPDATED_BY { get; set; }
        public string UPDATED_DT { get; set; }
        
    }

}