﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace AI070.Models
{
    public class LookupData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}