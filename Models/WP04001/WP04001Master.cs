﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP04001Master
{
    public class WP04001Master
    {
        public string ID { get; set; }
        public string AUTHORIZATION_NAME { get; set; }
        public string AUTHORIZATION_ST { get; set; }
        public string AUTHORIZATION_DESC { get; set; }
        public string ID_MENU { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string UPDATED_BY { get; set; }
        public string UPDATED_DT { get; set; }
        public string STATUS { get; set; }
        public string STATUS_ID { get; set; }
        public Int32 ROW_NUM { get; set; }
        public Int32 Number { get; set; }
        public string DELETE_DATA { get; set; }
        public string STACK { get; set; }
        public string LINE_STS { get; set; }
    }

}