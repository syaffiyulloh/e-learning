﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AD021.Models.TMS040500B
{
    public class TMS040500BDivisionOfTrainee
    {
        public string TRAINING_SCH_ID { get; set; }
        public string DIVISION { get; set; }
        public string DEPARTEMENT { get; set; }
    }
}