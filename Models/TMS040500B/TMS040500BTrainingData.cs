﻿/************************************************************************************************************
 * Program History :                                                                                        *
 *                                                                                                          *
 * Project Name     : TRAINING PLAINING & REGISTRATION ONLINE                                               *
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)                                      *
 * Function Id      : TMS040500W                                                                            *
 * Function Name    : Email Sending Batch                                                                   *
 * Function Group   : Registration Control                                                                  *
 * Program Id       : TMS040500BTrainingData                                                                *
 * Program Name     : Emai Sending Batch Training Data Class Model                                          *
 * Program Type     : Model                                                                                 *
 * Description      :                                                                                       *
 * Environment      : .NET 4.0, ASP MVC 4.0                                                                 *
 * Author           : FID.Arri                                                                              *
 * Version          : 01.00.00                                                                              *
 * Creation Date    : 17/12/2015 11:51:40                                                                   *
 *                                                                                                          *
 * Update history		Re-fix date				Person in charge				Description					*
 *                                                                                                          *
 * Copyright(C) 2015 - Fujitsu Indonesia. All Rights Reserved                                               *
 ************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AD021.Models.TMS040500B
{
    public class TMS040500BTrainingData
    {
        public string TRAINING_SCH_ID { get; set; }
        public string REG_PERIOD_ID { get; set; }
        public string TRAINING_TYPE_ID { get; set; }
        public int TRAINING_TOPIC_SEQ { get; set; }
        public string TRAINING_TOPIC_CD { get; set; }
        public string TRAINING_TOPIC_NM { get; set; }
        public DateTime TRAINING_SCH_FR { get; set; }
        public DateTime TRAINING_SCH_TO { get; set; }
        public string TRAINING_SHIFT { get; set; }
        public string TRAINING_SHIFT_NAME { get; set; }
        public int PASSING_POSTSCORE { get; set; }
        public int PASSING_ATTENDANCE { get; set; }
        public string KANBAN_MONTH { get; set; }
        public string LOCATION { get; set; }
        public string ROOM { get; set; }
        public string TRAINING_ADMIN { get; set; }
        public string TRAINING_SCH_STS { get; set; }
    }
}
