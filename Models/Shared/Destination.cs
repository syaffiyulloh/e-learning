﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.Shared
{
    public class Destination
    {
        public string DEST_CD { get; set; }
        public string DEST_NAME { get; set; }
    }
}