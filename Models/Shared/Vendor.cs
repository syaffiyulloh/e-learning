﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.Shared
{
    public class Vendor
    {
        public string VENDOR_CD { get; set; }
        public string VENDOR_NAME { get; set; }
        public string SEARCH_TERM { get; set; }
        public string ADDRESS_VENDOR { get; set; }
        public string SALES_PIC { get; set; }
        public string PHONE_VENDOR { get; set; }
        public string EMAIL_VENDOR { get; set; }
    }
}