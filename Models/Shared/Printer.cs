﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.Shared
{
    public class Printer
    {
        public string PRINTER_TYPE_ID { get; set; }
        public string PRINTER_TYPE_NM { get; set; }
    }
}