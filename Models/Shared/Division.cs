﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.Shared
{
    public class Division
    {
      public string PLANT_CD { get; set; }
      public string DIV_ID { get; set; }
      public string COORDINATOR_ID { get; set; }
      public string DIV_NAME { get; set; }
      public string ORG_ID { get; set; }
      public string COST_CENTER { get; set; }
      public string CREATED_BY { get; set; }
      public string CREATED_DT { get; set; }
      public string CHANGED_BY { get; set; }
      public string CHANGED_DT { get; set; }
    }
}