﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP03005
{
	public class WP03005Master
	{
        public int ID { get; set; }
        public int ROWNUM { get; set; }
        public string REG_NO { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string COMPANY_NAME { get; set; }
        public string USERNAME { get; set; }
        public string EMAIL { get; set; }
        public string IDENTITY_TYPE { get; set; }
        public string IDENTITY_NO { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string SAFETY_INDUCTION_NO { get; set; }
        public string SAFETY_DATE_START { get; set; }
        public string SAFETY_DATE_END { get; set; }
        public int IS_DELETED { get; set; }
    }

    public class WP03005Relation
    {
        public int ID { get; set; }
    }

    public class KeyList
    {
        public int ID { get; set; }
        public string REG_NO { get; set; }
        public string COMPANY { get; set; }
    }

}