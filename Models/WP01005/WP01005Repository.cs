﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using Toyota.Common.Credential;
using AI070.Models;

namespace AI070.Models.WP01005Master
{
    public class WP01005Repository
    {
        #region Get_Data_Grid_WP01005
        public List<WP01005Master> getDataWP01005(int Start, int Display, string EMPLOYEE_NAME)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP01005Master>("WP01005/WP01005_SearchData", new
            {
                START = Start,
                DISPLAY = Display,
                EMPLOYEE_NAME
            });
            db.Close();
            return d.ToList();
        }
        #endregion

        #region Count_Get_Data_Grid_WP01005
        public int getCountWP01005(string DATA_ID, string TIME_UNIT_CRITERIA, string EXECUTION_TIME, string STATUS_ACTIVE, string EMPLOYEE_NAME)
        {

            IDBContext db = DatabaseManager.Instance.GetContext();
            int result = db.SingleOrDefault<int>("WP01005/WP01005_SearchCount", new
            {
                DATA_ID = DATA_ID,
                TIME_UNIT_CRITERIA = TIME_UNIT_CRITERIA,
                EXECUTION_TIME = EXECUTION_TIME,
                STATUS_ACTIVE = STATUS_ACTIVE,
                EMPLOYEE_NAME
            });
            db.Close();
            return result;
        }
        #endregion

        #region Delete Data
        public void Delete_Data(string ID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Execute("WP01005/WP01005_Delete", new
            {
                ID
            });
            db.Close();
        }
        #endregion

        #region Update Data
        public List<WP01005> Update_Data(string ID, string NOREG, string COMPANY, string FIRSTNAME, string LASTNAME, string USERNAME, string PASSWORD, string EMAIL, string ADDRESS,string PHONE,string IDENTITY_TYPE,string IDENTITY_NO,string PIC_STATUS,string ANZEN_NO,string ANZEN_DT_FROM,string ANZEN_DT_TO,string SAFETY_INDUCTION_NO,string SAFETY_INDUCTION_FROM,string SAFETY_INDUCTION_TO,string SECTION, string STATUS,string username)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP01005>("WP01005/WP01005_Update", new
            {
                ID,
                NOREG,
                COMPANY,
                FIRSTNAME,
                LASTNAME,
                USERNAME,
                PASSWORD,
                EMAIL,
                ADDRESS,
                PHONE,
                IDENTITY_TYPE,
                IDENTITY_NO,
                PIC_STATUS,
                ANZEN_NO,
                ANZEN_DT_FROM,
                ANZEN_DT_TO,
                SAFETY_INDUCTION_NO,
                SAFETY_INDUCTION_FROM,
                SAFETY_INDUCTION_TO,
                SECTION,
                STATUS,
                username
            });
            db.Close();
            return d.ToList();
        }
        #endregion

        public static List<WP01005> Create(string COMPANY, string REGNUMBER, string USERNAME, string PASSWORD, string EMAIL, string SECTION, string FIRST_NAME,
           string LAST_NAME, string ADDRESS, string PHONE, string IDENTITY_TYPE, string IDENTITY_NO, string PIC_STATUS, string ANZEN_NO, string ANZEN_DT_FROM, string ANZEN_DT_TO, string SAFETY_INDUCTION_NO, string SAFETY_INDUCTION_FROM, string SAFETY_INDUCTION_TO, string SEQ_NO, string STATUS, string username)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<WP01005>("WP01005/WP01005_Create", new
            {
                COMPANY,
                REGNUMBER,
                FIRST_NAME,
                LAST_NAME,
                USERNAME,
                PASSWORD, 
                EMAIL,
                SECTION,
                ADDRESS,
                PHONE,
                IDENTITY_TYPE,
                IDENTITY_NO,
                ANZEN_NO,
                ANZEN_DT_FROM,
                ANZEN_DT_TO,
                SAFETY_INDUCTION_NO,
                SAFETY_INDUCTION_FROM,
                SAFETY_INDUCTION_TO,
                SEQ_NO,
                STATUS,
                PIC_STATUS,
                username
            });
            db.Close();
            return d.ToList();
        }

        #region Get Project Location
        public List<CompanyModel> getCompany()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<CompanyModel>("WP01005/WP01005_getCompany");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Identity
        public List<IdentityModel> getIdentity()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<IdentityModel>("WP01005/WP01005_getIdentity");

            db.Close();
            return d.ToList();
        }
        #endregion

        #region Get Section
        public List<SectionModel> getSection(string SYSTEM_CD)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<SectionModel>("WP01005/WP01005_getSection", new {
                SYSTEM_CD
            });

            db.Close();
            return d.ToList();
        }



        #endregion

        #region Get PIC
        public List<PICModel> getPIC()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<PICModel>("WP01005/WP01005_getPic");

            db.Close();
            return d.ToList();
        }
        #endregion



        #region Get Executor
        public List<ExecutorModel> getExecutor()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            var d = db.Fetch<ExecutorModel>("WP01005/WP01005_getExecutor");

            db.Close();
            return d.ToList();
        }
        #endregion
    }

    public class StatusModel
    {
        public string ID { get; set; }
        public string Status { get; set; }
    }

    public class DivisionModel
    {
        public string Division { get; set; }
    }

    public class CompanyModel
    {
        public string ID { get; set; }
        public string COMPANY { get; set; }
    }

    public class IdentityModel
    {
        public string ID { get; set; }
        public string IDENTITY_TEXT { get; set; }
    }

    public class PICModel
    {
        public string ID { get; set; }
        public string PIC_TEXT { get; set; }
    }

    public class SectionModel
    {
        public string ID { get; set; }
        public string SECTION_TEXT { get; set; }
    }

    public class ExecutorModel
    {
        public string ID { get; set; }
        public string EXECUTOR_TEXT { get; set; }
    }

    public class PagingModel_WP01005
    {
        public int CountData { get; set; }
        public int StartData { get; set; }
        public int EndData { get; set; }
        public int PositionPage { get; set; }
        public List<int> ListIndex { get; set; }
        public PagingModel_WP01005(int countdata, int positionpage, int dataperpage)
        {
            List<int> list = new List<int>();
            EndData = positionpage * dataperpage;
            CountData = countdata;
            PositionPage = positionpage;
            StartData = (positionpage - 1) * dataperpage + 1;
            Double jml = countdata / dataperpage;
            if (countdata % dataperpage > 0)
            {
                jml = jml + 1;
            }

            for (int i = 0; i < jml; i++)
            {
                list.Add(i);
            }
            ListIndex = list;
        }
    }
}