﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP01005Master
{
    public class WP01005Master
    {
        public string ID { get; set; }
        public string NOREG { get; set; }
        public string COMPANY { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string EMAIL { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string IDENTITY_TYPE { get; set; }
        public string IDENTITY_NO { get; set; }
        public string LEADER_ID { get; set; }
        public string PIC_STATUS { get; set; }
        public string ANZEN_SERTIFICATE_NO { get; set; }
        public string ANZEN_DT_FROM { get; set; }
        public string ANZEN_DT_TO { get; set; }
        public string SAFETY_INDUCTION_NO { get; set; }
        public string SAFETY_INDUCTION_FROM { get; set; }
        public string SAFETY_INDUCTION_TO { get; set; }
        public string SEQ_NO { get; set; }
        public string STATUS { get; set; }
        public string STATUSTEXT
        {
            get; set;
        }
        public string SECTION { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DT { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DT { get; set; }
        public string ID_TB_M_COMPANY { get; set; }
        public string STATUS_ID { get; set; }
        public Int32 ROW_NUM { get; set; }
        public Int32 Number { get; set; }
        public string DELETE_DATA { get; set; }
        public string STACK { get; set; }
        public string LINE_STS { get; set; }

    }

}