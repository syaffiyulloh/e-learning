﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models
{
    public class ResultMessage
    {
        public string result  { get; set; }
        public string message { get; set; }
        public string messageDetail { get; set; }
    }
}
