﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AI070.Models.WP03004Master
{
    public class WP03004Master
    {
        public int ROW_NUM { get; set; }
        public string ID { get; set; }
        public string TITLE { get; set; }
        public string DESCRIPTION { get; set; }
        public string CONTENT_TRAINING { get; set; }
        public string FILE_NAME { get; set; }
    }

    public class TrainingDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Title.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter Description.")]
        public string Description { get; set; }
        public string FilePath { get; set; }
        public HttpPostedFileBase File { get; set; }

        [Required(ErrorMessage = "Please enter Training content.")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        public string FileName { get; set; }
    }

}