﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.WP01004Master
{
    public class WP01004
    {
        public string WORKING_NAME { get; set; }
        public string STATUS { get; set; }
        public string TIME_CD { get; set; }
        public string TIME_VAL { get; set; }
        public string STS_CD { get; set; }
        public string STS_VAL { get; set; }
        public string STACK { get; set; }
        public string LINE_STS { get; set; }

    }

}