﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models
{
    public class Lookup
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Display { get; set; }        
    }
}