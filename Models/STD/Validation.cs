﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.STD
{
    public class Validation : BaseModel
    {
        /// <summary>
        /// Result
        /// </summary>
        public bool? Result { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Param
        /// </summary>
        public string Param { get; set; }
    }
}