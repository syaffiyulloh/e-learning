﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AI070.Models.STD
{
    public class FTPCon
    {
        public string uri { get; set; }
        public string pass { get; set; }
        public string username { get; set; }
    }
}