﻿UPDATE [dbo].[TB_R_LEARN_REG_PROJ_EMPLOYEE]
   SET [ID_TB_R_WP_PROJECT] = @ProjectId
      ,[ID_TB_M_COMPANY] = @CompanyId
      ,[COMPANY_FROM] = @CompanyFromId
      ,[COMPANY_TO] = @CompanyToId
      ,[IS_PROGRESS] = @IsProgress
      ,[UPDATED_BY] = @ModifiedBy
      ,[UPDATED_DT] = GETDATE()
 WHERE [ID_TB_R_LEARN_REG_PROJ_EMPLOYEE] = @Id