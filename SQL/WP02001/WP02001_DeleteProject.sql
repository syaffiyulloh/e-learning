﻿DECLARE @@CNT INT
	, @@CHK VARCHAR(20)
	, @@ERR VARCHAR(MAX);
DECLARE @@MSG_TEXT VARCHAR(MAX);
DECLARE @@MSG_TYPE VARCHAR(MAX);

BEGIN TRY
	SET @@CNT = (SELECT COUNT(1) FROM TB_R_WP_PROJECT WHERE ID = @WP_PROJECT_ID);
	IF(@@CNT > 0)
	BEGIN

		UPDATE TB_R_WP_PROJECT 
			SET 
				PROJECT_STATUS = 'DELETED',
				[CHANGED_BY] = @username,
				[CHANGED_DT] = GETDATE()
		WHERE ID = @WP_PROJECT_ID;

		SET @@CHK = 'TRUE';
		SET @@ERR = 'NOTHING';

	END 
	ELSE
	BEGIN
		SET @@CHK = 'FALSE';
		SET @@ERR = 'ERROR DELETE DATA, PROJECT UNAVAILABLE!';
	END
END TRY
BEGIN CATCH
	SET @@CHK = 'FALSE';
	SET @@ERR = 'ERROR INSERT WP_PROJECT:' +@WP_PROJECT_ID+
	'<br/>Detail Error :|: ' + ERROR_MESSAGE() + ' :|: ';	
END CATCH

SELECT @@CHK AS STACK, @@ERR AS LINE_STS,CAST(@@CNT AS VARCHAR(20)) AS WP_PROJECT_ID