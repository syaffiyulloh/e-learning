﻿DECLARE @@CNT INT
	, @@CHK VARCHAR(20)
	, @@ERR VARCHAR(MAX);
BEGIN TRY
	UPDATE TB_M_EMPLOYEE SET
	    ID_TB_M_COMPANY = @COMPANY,
		REG_NO = @NOREG,
		FIRST_NAME = @FIRSTNAME,
		LAST_NAME = @LASTNAME,
		USERNAME = @USERNAME,
		PASSWORD = @PASSWORD,
		EMAIL = @EMAIL,
		ADDRESS = @ADDRESS,
		PHONE = @PHONE,
		IDENTITY_TYPE = @IDENTITY_TYPE,
		IDENTITY_NO = @IDENTITY_NO,
		PIC_STATUS = @PIC_STATUS,
		ANZEN_SERTIFICATE_NO = @ANZEN_NO,
		ANZEN_DT_FROM = @ANZEN_DT_FROM,
		ANZEN_DT_TO = @ANZEN_DT_TO,
		SAFETY_INDUCTION_NO = @SAFETY_INDUCTION_NO,
		SAFETY_INDUCTION_FROM = @SAFETY_INDUCTION_FROM,
		SAFETY_INDUCTION_TO = @SAFETY_INDUCTION_TO,
		SECTION = @SECTION,
		STATUS = @STATUS,
		UPDATE_BY = @USERNAME, 
		UPDATE_DT = GETDATE() 
	WHERE ID_TB_M_EMPLOYEE = @ID
	
	SET @@CHK = 'TRUE';
	SET @@ERR = 'Data Has Been Updated';
END TRY
BEGIN CATCH
	SET @@CHK = 'FALSE';
	SET @@ERR = 'ERROR UPDATE TB_M_EMPLOYEE: ' +@IDENTITY_NO+
	'<br/>Detail Error :|: ' + ERROR_MESSAGE() + ' :|: ';	
END CATCH

SELECT @@CHK AS STACK, @@ERR AS LINE_STS