﻿DECLARE @@CNT INT
	, @@CHK VARCHAR(20)
	, @@ERR VARCHAR(MAX);
BEGIN TRY
	UPDATE TB_M_LOCATION SET ID_TB_M_AREA = @AREA_CODE, LOC_CD = @LOC_CD, LOC_NAME = @LOC_NAME, LONG = @LONG, LAT = @LAT, WIDE = @WIDE, POINT = @POINT, UPDATE_BY = @USERNAME, UPDATE_DT = GETDATE() WHERE ID_TB_M_LOCATION = @ID
	SET @@CHK = 'TRUE';
	SET @@ERR = 'Data Has Been Updated';
END TRY
BEGIN CATCH
	SET @@CHK = 'FALSE';
	SET @@ERR = 'ERROR UPDATE TB_M_LOCATION:' +@LOC_CD+
	'<br/>Detail Error :|: ' + ERROR_MESSAGE() + ' :|: ';	
END CATCH

SELECT @@CHK AS STACK, @@ERR AS LINE_STS