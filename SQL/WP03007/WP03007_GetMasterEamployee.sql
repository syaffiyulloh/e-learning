﻿SELECT [ID_TB_M_EMPLOYEE] AS [Id]
      ,[ID_TB_M_COMPANY] AS [CompanyId]
      ,[FIRST_NAME] + ' ' + [LAST_NAME] AS [Name]
  FROM [dbo].[TB_M_EMPLOYEE]
  WHERE [ID_TB_M_EMPLOYEE] = @ID OR @ID = 0