﻿SELECT [ID_TB_M_LEARN_MOD_TRAINING] AS [Id]
      ,[TITLE] AS [Title]
      ,[DESCRIPTION] AS [Description]
      ,[FILE_NAME] AS [FilePath]
	  ,[CONTENT_TRAINING] AS [Content]
      ,[FILE_NAME] AS [FileName]
  FROM [dbo].[TB_M_LEARN_MODULE_TRAINING]
  WHERE [ID_TB_M_LEARN_MOD_TRAINING] = @id