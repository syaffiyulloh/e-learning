﻿DECLARE @@QUERY VARCHAR(MAX);
DECLARE @@START VARCHAR(50) = @START;
DECLARE @@DISPLAY VARCHAR(50) = @DISPLAY;


SET @@QUERY = '';
SET @@QUERY = 'SELECT 
	*
 FROM 
(
	SELECT ROW_NUMBER() OVER (ORDER BY SYSTEM_ID ASC) ROW_NUM,
	SYSTEM_ID,
	SYSTEM_TYPE,
	SYSTEM_CD,
	convert(varchar(10), SYSTEM_VALID_FR, 120) AS SYSTEM_VALID_FR,
	convert(varchar(10), SYSTEM_VALID_TO, 120) AS SYSTEM_VALID_TO,
	SYSTEM_VALUE_TXT,
	SYSTEM_VALUE_NUM,
	convert(varchar(10), SYSTEM_VALUE_DT, 120) AS SYSTEM_VALUE_DT,
	CREATED_BY,
	CREATED_DT,
	CHANGED_BY,
	CHANGED_DT 
	FROM TB_M_SYSTEM
	WHERE 1=1 AND IS_DELETED = 0
';


IF(@SYSTEM_ID <> '')
	BEGIN
		SET @@QUERY = @@QUERY + 'AND SYSTEM_ID LIKE ''%'+RTRIM(@SYSTEM_ID)+'%'' ';
	END

IF(@SYSTEM_TYPE <> '')
	BEGIN
		SET @@QUERY = @@QUERY + 'AND SYSTEM_TYPE LIKE ''%'+RTRIM(@SYSTEM_TYPE)+'%'' ';
	END

IF(@SYSTEM_VALUE_TEXT <> '')
	BEGIN
		SET @@QUERY = @@QUERY + 'AND SYSTEM_VALUE_TXT LIKE ''%'+RTRIM(@SYSTEM_VALUE_TEXT)+'%'' ';
	END

IF(@SYSTEM_VALUE_NUM <> '')
	BEGIN
		SET @@QUERY = @@QUERY + 'AND SYSTEM_VALUE_NUM LIKE ''%'+RTRIM(@SYSTEM_VALUE_NUM)+'%'' ';
	END

IF(@SYSTEM_FROM <> '' AND @SYSTEM_TO <> '')
	BEGIN
		SET @@QUERY = @@QUERY + 'AND SYSTEM_VALUE_DT BETWEEN '''+@SYSTEM_FROM+''' AND '''+@SYSTEM_TO+''' ';
	END

SET @@QUERY = @@QUERY +') as TB';

IF(@@START > 0 AND @@DISPLAY > 0)
BEGIN	
	SET @@QUERY = @@QUERY +' WHERE ROW_NUM BETWEEN '+@@START+' AND '''+@@DISPLAY+''' ';
END

EXEC(@@QUERY)