﻿SELECT E.[ID_TB_M_EMPLOYEE] AS Id
    ,CONCAT(E.FIRST_NAME, ' ', E.LAST_NAME) AS [Name]
	,E.[ADDRESS] AS [Address]
	,C.COMPANY_NAME AS Company
    ,E.IDENTITY_NO AS IdentityNo
FROM [TB_M_EMPLOYEE] AS E
	LEFT JOIN TB_M_COMPANY AS C
		ON E.ID_TB_M_COMPANY = C.ID_TB_M_COMPANY
WHERE E.ID_TB_M_EMPLOYEE = @ID